from datetime import datetime
from pydantic.main import BaseModel
from typing import Optional

class CreatePasswordResponse(BaseModel):
    aid: Optional[str]
    uid: Optional[str]
    email: Optional[str]
    first_name: Optional[str]
    last_name: Optional[str]
    status: Optional[str]


CreatePasswordResponse.update_forward_refs()
