from datetime import datetime
from pydantic.main import BaseModel
from typing import Optional

class SocialLinkingResponse(BaseModel):
    identity_social_linking_state: Optional[str]
    password_confirmation_available: Optional[bool]
    apple_confirmation_available: Optional[bool]
    facebook_confirmation_available: Optional[bool]
    google_confirmation_available: Optional[bool]
    twitter_confirmation_available: Optional[bool]
    linked_in_confirmation_available: Optional[bool]
    email: Optional[str]
    first_name: Optional[str]
    last_name: Optional[str]
    social_type: Optional[str]
    is_passwordless: Optional[bool]


SocialLinkingResponse.update_forward_refs()
