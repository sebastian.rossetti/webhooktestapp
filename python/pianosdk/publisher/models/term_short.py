from datetime import datetime
from pydantic.main import BaseModel
from typing import Optional

class TermShort(BaseModel):
    term_id: Optional[str]
    name: Optional[str]
    disabled: Optional[bool]


TermShort.update_forward_refs()
