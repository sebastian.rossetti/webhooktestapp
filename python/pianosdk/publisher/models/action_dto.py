from datetime import datetime
from pydantic.main import BaseModel
from typing import Optional
from pianosdk.publisher.models.change_subscription_term_step import ChangeSubscriptionTermStep
from pianosdk.publisher.models.send_email_step import SendEmailStep

class ActionDTO(BaseModel):
    action_id: Optional[str]
    type: Optional[str]
    number_of_items: Optional[int]
    status: Optional[str]
    schedule_time: Optional[datetime]
    change_subscription_term_step: Optional['ChangeSubscriptionTermStep']
    readonly: Optional[bool]
    send_email_step: Optional['SendEmailStep']


ActionDTO.update_forward_refs()
