from datetime import datetime
from pydantic.main import BaseModel
from typing import Optional

class ExperienceConfigVersion(BaseModel):
    version: Optional[int]
    js: Optional[str]
    published: Optional[bool]
    published_date: Optional[datetime]


ExperienceConfigVersion.update_forward_refs()
