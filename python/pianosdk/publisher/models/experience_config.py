from datetime import datetime
from pydantic.main import BaseModel
from typing import Optional
from pianosdk.publisher.models.experience_config_version import ExperienceConfigVersion

class ExperienceConfig(BaseModel):
    experience_config_id: Optional[str]
    aid: Optional[str]
    create_date: Optional[datetime]
    create_by: Optional[int]
    update_date: Optional[datetime]
    update_by: Optional[int]
    published_version: Optional['ExperienceConfigVersion']


ExperienceConfig.update_forward_refs()
