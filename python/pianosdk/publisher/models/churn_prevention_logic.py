from datetime import datetime
from pydantic.main import BaseModel
from typing import Optional

class ChurnPreventionLogic(BaseModel):
    logic_id: Optional[str]
    name: Optional[str]
    description: Optional[str]
    create_date: Optional[datetime]
    update_date: Optional[datetime]


ChurnPreventionLogic.update_forward_refs()
