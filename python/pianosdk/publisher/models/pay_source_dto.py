from datetime import datetime
from pydantic.main import BaseModel
from typing import Optional

class PaySourceDTO(BaseModel):
    id: Optional[int]
    identifier: Optional[str]
    caption: Optional[str]
    title: Optional[str]


PaySourceDTO.update_forward_refs()
