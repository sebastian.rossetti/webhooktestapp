from datetime import datetime
from pydantic.main import BaseModel
from typing import Optional

class Mandate(BaseModel):
    id: Optional[str]
    reference: Optional[str]
    next_charge_date: Optional[datetime]


Mandate.update_forward_refs()
