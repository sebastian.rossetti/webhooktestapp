from datetime import datetime
from io import StringIO
from typing import TextIO, Dict, List, Union

from pianosdk.api_response import ApiResponse
from pianosdk.base_api import BaseApi
from pianosdk.configuration import Configuration
from pianosdk.httpwrap import HttpCallBack
from pianosdk.utils import _json_deserialize, _encode_parameter
from pianosdk.publisher.models.term import Term


class PublisherTermDynamicApi(BaseApi):
    def __init__(self, config: Configuration, http_callback: HttpCallBack = None) -> None:
        super().__init__(config, http_callback)

    def create_dynamic_term(self, aid: str, rid: str, name: str, payment_currency: str, billing_configuration: str, description: str = None) -> ApiResponse[Term]:
        _url_path = '/api/v3/publisher/term/dynamic/create'
        _query_url = self.config.get_base_url() + _url_path
        _query_parameters = {

        }

        _headers = {
            'api_token': self.config.api_token,
            'Accept': 'application/json'
        }

        _parameters = {
            'aid': _encode_parameter(aid),
            'rid': _encode_parameter(rid),
            'name': _encode_parameter(name),
            'payment_currency': _encode_parameter(payment_currency),
            'billing_configuration': _encode_parameter(billing_configuration),
            'description': _encode_parameter(description)
        }

        _files = {
        }

        _request = self.config.http_client.build_request('POST',
                                                         _query_url,
                                                         headers=_headers,
                                                         query_parameters=_query_parameters,
                                                         parameters=_parameters,
                                                         files=_files)
        _response = self._execute_request(_request)
        _result = _json_deserialize(_response, Term)
        return _result

    def update_dynamic_term(self, aid: str, term_pub_id: str, rid: str, name: str, payment_currency: str, billing_configuration: str, description: str = None, payment_new_customers_only: bool = False, payment_allow_promo_codes: bool = False) -> ApiResponse[Term]:
        _url_path = '/api/v3/publisher/term/dynamic/update'
        _query_url = self.config.get_base_url() + _url_path
        _query_parameters = {

        }

        _headers = {
            'api_token': self.config.api_token,
            'Accept': 'application/json'
        }

        _parameters = {
            'aid': _encode_parameter(aid),
            'term_pub_id': _encode_parameter(term_pub_id),
            'rid': _encode_parameter(rid),
            'name': _encode_parameter(name),
            'payment_currency': _encode_parameter(payment_currency),
            'billing_configuration': _encode_parameter(billing_configuration),
            'description': _encode_parameter(description),
            'payment_new_customers_only': _encode_parameter(payment_new_customers_only),
            'payment_allow_promo_codes': _encode_parameter(payment_allow_promo_codes)
        }

        _files = {
        }

        _request = self.config.http_client.build_request('POST',
                                                         _query_url,
                                                         headers=_headers,
                                                         query_parameters=_query_parameters,
                                                         parameters=_parameters,
                                                         files=_files)
        _response = self._execute_request(_request)
        _result = _json_deserialize(_response, Term)
        return _result

