from datetime import datetime
from pydantic.main import BaseModel
from typing import Optional
from pianosdk.anon.models.resource import Resource

class ConfirmationResult(BaseModel):
    resource: Optional['Resource']
    show_offer_params: Optional[str]
    type: Optional[str]


ConfirmationResult.update_forward_refs()
