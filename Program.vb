Imports System.Security.Cryptography
Imports System.Text
Imports Newtonsoft.Json.Linq

Module Program
    Sub Main(args As String())
        Dim PrivateKey As String = "PUT_PROVIDED_PRIVATE_KEY_HERE"
        Dim WebhookDataArray(10) As String

        WebhookDataArray = New String() {
            "LZ2Tki2z38MU9P4Vcie9k9BEr96Qq-8FowfWmcuH1xVs0NjDwdKCGKdE_p2Ryi4OAax7digla88dGtCg91T7wMz8QVQOer60VlRDx5evL1YkYX22SdGYBvHJ6UMG3Fx1xPOXD2AaACNtWAi6auqw6XvMHxXwyY490fdsf3HIfESQdkUm8iZ97wXGCNLvMSj841vTDNZPD3D4cBzYlEYyelEpCFXhOX49rKfe5wlqZfh_BuCcIKrsM1JtNBg-DpyCCs0vdchS0GAxUPQQUFFAYg~~~iig_paVDpWffc_YhPH24x_5NMsD0OLJOXUACH2Jg804",
            "LZ2Tki2z38MU9P4Vcie9k9BEr96Qq-8FowfWmcuH1xVs0NjDwdKCGKdE_p2Ryi4OAax7digla88dGtCg91T7wMz8QVQOer60VlRDx5evL1YkYX22SdGYBvHJ6UMG3Fx1xPOXD2AaACNtWAi6auqw6XvMHxXwyY490fdsf3HIfEQPH9uZ1dIs40Qnc6yDcMX5kXoDHjn04Z7zFgUM_r36JqMAlrrUMIsblaaD-iQIwKp_BuCcIKrsM1JtNBg-DpyCE7zrmLNXB_cskQZesBHwtw~~~WXdJNMDZcJVGpsa1tUuUr3Jkh8SuXGUwaJG0E22pEyM",
            "LZ2Tki2z38MU9P4Vcie9k9BEr96Qq-8FowfWmcuH1xVs0NjDwdKCGKdE_p2Ryi4OAax7digla88dGtCg91T7wMz8QVQOer60VlRDx5evL1YkYX22SdGYBvHJ6UMG3Fx1xPOXD2AaACNtWAi6auqw6XvMHxXwyY490fdsf3HIfEQNkQ4OKCfXU8W6_BA7dcjW41lnrW_54aoGX9FV7oBsdAMGV0eZI4Hs_MtkhqmH6ih_BuCcIKrsM1JtNBg-DpyCRvKXdsv9LhMOvtTPDrDwhQ~~~6El4LfWbdBae_TMOijHcVU0-_qvfmhIz-oBWzHj2usg",
            "LZ2Tki2z38MU9P4Vcie9k9BEr96Qq-8FowfWmcuH1xVs0NjDwdKCGKdE_p2Ryi4OAax7digla88dGtCg91T7wMz8QVQOer60VlRDx5evL1YkYX22SdGYBvHJ6UMG3Fx1xPOXD2AaACNtWAi6auqw6XvMHxXwyY490fdsf3HIfETi-kE5n1UaSwsGmYain63OgeWWC8ZIRwKfD2uvWbN4qCEGkTPVoqjZgv4bZrSSg3p_BuCcIKrsM1JtNBg-DpyCbJfklZs9IAwh3ZyMhIGmAA~~~I-Oo6vB0N9fMAc3gBvCnHwrw6BhhVxGm_jZtMqa5PLc",
            "LZ2Tki2z38MU9P4Vcie9k9BEr96Qq-8FowfWmcuH1xVs0NjDwdKCGKdE_p2Ryi4OAax7digla88dGtCg91T7wMz8QVQOer60VlRDx5evL1YkYX22SdGYBvHJ6UMG3Fx1xPOXD2AaACNtWAi6auqw6XvMHxXwyY490fdsf3HIfER6eB7qjkqDjZS7eb76TcEH1c4-E_5kpzh2CA-a5t5Krap9ByaBfKrZAzJZMrhXPkF_BuCcIKrsM1JtNBg-DpyC9P0vFfv1oYmcXhfxLi8uTg~~~M3YvDXGGigfLIyDXMLdHu8CJ3lUA1StHxKs_1AhP2Jk",
            "LZ2Tki2z38MU9P4Vcie9k9BEr96Qq-8FowfWmcuH1xVs0NjDwdKCGKdE_p2Ryi4OAax7digla88dGtCg91T7wMz8QVQOer60VlRDx5evL1YkYX22SdGYBvHJ6UMG3Fx1xPOXD2AaACNtWAi6auqw6XvMHxXwyY490fdsf3HIfER9ggXrflVvlKj1FznU6orTbQBEHrIye3Zu5SGymG3wSLzyBVZ-u0zfkciGYlemCUB_BuCcIKrsM1JtNBg-DpyCi3ZZNBNntQAcqlmFdESKqA~~~NIOa-nY7kNE2rIz1rsRWC6piSeYsCy9Tt9CucJ_vLbk",
            "LZ2Tki2z38MU9P4Vcie9k9BEr96Qq-8FowfWmcuH1xVs0NjDwdKCGKdE_p2Ryi4OAax7digla88dGtCg91T7wMz8QVQOer60VlRDx5evL1YkYX22SdGYBvHJ6UMG3Fx1xPOXD2AaACNtWAi6auqw6XvMHxXwyY490fdsf3HIfER6eB7qjkqDjZS7eb76TcEH1c4-E_5kpzh2CA-a5t5Krap9ByaBfKrZAzJZMrhXPkF_BuCcIKrsM1JtNBg-DpyC9P0vFfv1oYmcXhfxLi8uTg~~~M3YvDXGGigfLIyDXMLdHu8CJ3lUA1StHxKs_1AhP2Jk",
            "LZ2Tki2z38MU9P4Vcie9k9BEr96Qq-8FowfWmcuH1xVs0NjDwdKCGKdE_p2Ryi4OAax7digla88dGtCg91T7wMz8QVQOer60VlRDx5evL1YkYX22SdGYBvHJ6UMG3Fx1xPOXD2AaACNtWAi6auqw6XvMHxXwyY490fdsf3HIfER6eB7qjkqDjZS7eb76TcEH1c4-E_5kpzh2CA-a5t5Krap9ByaBfKrZAzJZMrhXPkF_BuCcIKrsM1JtNBg-DpyC9P0vFfv1oYmcXhfxLi8uTg~~~M3YvDXGGigfLIyDXMLdHu8CJ3lUA1StHxKs_1AhP2Jk",
            "LZ2Tki2z38MU9P4Vcie9k9BEr96Qq-8FowfWmcuH1xVs0NjDwdKCGKdE_p2Ryi4OAax7digla88dGtCg91T7wMz8QVQOer60VlRDx5evL1YkYX22SdGYBvHJ6UMG3Fx1xPOXD2AaACNtWAi6auqw6XvMHxXwyY490fdsf3HIfER6eB7qjkqDjZS7eb76TcEH1c4-E_5kpzh2CA-a5t5Krap9ByaBfKrZAzJZMrhXPkF_BuCcIKrsM1JtNBg-DpyC9P0vFfv1oYmcXhfxLi8uTg~~~M3YvDXGGigfLIyDXMLdHu8CJ3lUA1StHxKs_1AhP2Jk",
            "LZ2Tki2z38MU9P4Vcie9k9BEr96Qq-8FowfWmcuH1xVs0NjDwdKCGKdE_p2Ryi4OAax7digla88dGtCg91T7wMz8QVQOer60VlRDx5evL1YkYX22SdGYBvHJ6UMG3Fx1xPOXD2AaACNtWAi6auqw6XvMHxXwyY490fdsf3HIfER6eB7qjkqDjZS7eb76TcEH1c4-E_5kpzh2CA-a5t5Krap9ByaBfKrZAzJZMrhXPkF_BuCcIKrsM1JtNBg-DpyC9P0vFfv1oYmcXhfxLi8uTg~~~M3YvDXGGigfLIyDXMLdHu8CJ3lUA1StHxKs_1AhP2Jk"
        }

        Dim WebhookData As String

        For Each WebhookData In WebhookDataArray
            Console.WriteLine("WebhookData:" & vbTab & WebhookData)
            Console.WriteLine(DecryptWebhook(WebhookData, PrivateKey))
        Next WebhookData
    End Sub

    Function DecryptWebhook(WebhookData As String, PrivateKey As String) As JObject
        Dim Segments As String() = WebhookData.Split("~~~")
        Dim Data As String = Segments(0).ToString()
        Dim HMAC As String = Segments(1).ToString()

        'Console.WriteLine("WebhookData:" & vbTab & WebhookData)
        'Console.WriteLine("Data:" & vbTab & Data)
        'Console.WriteLine("HMAC:" & vbTab & HMAC)

        If Segments.Length > 2 Then
            Throw New Exception("Invalid message: We've found data after HMAC")
        End If

        ' Test received HMAC vs SHA256 generated one
        If HMAC <> "" Then
            Dim GeneratedHMAC As String = HashString(Data, PrivateKey)
            'Console.WriteLine("GeneratedHMAC:" & vbTab & GeneratedHMAC)

            If HMAC <> GeneratedHMAC Then
                Throw New Exception("Could not parse message: invalid HMAC")
            End If
        End If

        Dim JWT As String
        JWT = AESDecrypt(Data, PrivateKey)
        'Console.WriteLine("JWT:" & vbTab & JWT)

        Dim JSONString As String = Base64Decode(JWT)

        Return JObject.Parse(JSONString)
    End Function

    Public Function AESDecrypt(Value As String, Key As String) As String
        Dim Data As Object = DecodeStripped(Value)
        'Console.WriteLine("Data:" & vbTab & Convert.ToBase64String(Data))

        Dim CleanKey As String
        If Key.Length > 32 Then
            CleanKey = Key.Substring(0, 32)
        Else
            CleanKey = Key & String.Join("", Enumerable.Repeat("X", 32 - Key.Length))
        End If
        'Console.WriteLine("CleanKey:" & vbTab & CleanKey)

        Dim ByteKey As Byte() = Encoding.UTF8.GetBytes(CleanKey)
        Dim AESObj As Object = Aes.Create()
        Dim Decrypted As Byte()

        AESObj.Key = ByteKey
        AESObj.Mode = CipherMode.ECB
        AESObj.BlockSize = 128
        AESObj.Padding = PaddingMode.None

        Dim DESDecryptor As ICryptoTransform = AESObj.CreateDecryptor()
        Decrypted = DESDecryptor.TransformFinalBlock(Data, 0, Data.Length)

        Dim Padding As Byte = Decrypted.Last()

        If 0 < Padding And Padding <= 16 Then
            Decrypted = Decrypted.Take(Decrypted.Length - Padding).ToArray()
        End If

        Return Convert.ToBase64String(Decrypted)
    End Function

    Function Base64Decode(base64EncodedData As String) As String
        Dim base64EncodedBytes As Byte() = Convert.FromBase64String(base64EncodedData)
        Return Encoding.UTF8.GetString(base64EncodedBytes)
    End Function

    Function DecodeStripped(Value As String) As Object
        Dim Data As String = Replace(Replace(Value, "-", "+"), "_", "/")
        If Data.Length Mod 4 <> 0 Then
            Data = Data & String.Join("", Enumerable.Repeat("=", 4 - (Data.Length Mod 4)))
        End If
        Return Convert.FromBase64String(Data)
    End Function

    Function HashString(StringToHash As String, HashKey As String) As String
        Dim Encoder As New UTF8Encoding
        Dim Key() As Byte = Encoder.GetBytes(HashKey)
        Dim Text() As Byte = Encoder.GetBytes(StringToHash)
        Dim HMACSHA256 As New HMACSHA256(Key)
        Dim HashCode As Byte() = HMACSHA256.ComputeHash(Text)
        Dim Hash As String = Replace(Replace(Replace(Convert.ToBase64String(HashCode), "+", "-"), "/", "_"), "=", "")
        Return Hash
    End Function
End Module
